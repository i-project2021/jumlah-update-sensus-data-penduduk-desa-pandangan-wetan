.model small
.code
org 100h
jmp start
output1 db "Program Update Sensus Data Penduduk ",0dh,0ah, '$'
output2 db "Program Bekerja Hanya Dengan Angka Integer. ", 0dh,0ah, '$'
output3 db "Hanya Bekerja Optimal Dengan Angka Yang Tidak Terlalu Besar. ", 0dh, 0ah, '$'
nilai1 db 0dh,0ah, "Masukkan No. KK : ",'$'
nilai2 db 0dh,0ah, "Jumlah nama disetiap KK: ",'$'
hasil db 0dh,0ah, "Hasilnya Adalah = ",'$'
akhir db 0dh,0ah, "Terima Kasihhh",'$'
start:
 mov ah,09h
 mov dx,offset output1
 int 21h

 mov ah,09h
 mov dx,offset output2
 int 21h

 mov ah,09h
 mov dx,offset output3
 int 21h

 mov ah,09h
 mov dx,offset nilai1
 int 21h

 mov ah,09h
 mov dx,offset nilai2
 int 21h

 mov ah,09h
 mov dx,offset hasil
 int 21h

 mov ah,09h
 mov dx,offset akhir
 int 21h
